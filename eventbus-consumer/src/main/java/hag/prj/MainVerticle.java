package hag.prj;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.Message;

import java.util.List;

public class MainVerticle extends AbstractVerticle {
    @Override
    public void start() throws Exception {

        vertx.eventBus().consumer("hello",msg->{
            msg.reply(new JsonObject().put("msg","hello").put("served-by",this.toString()));});
    }
}
