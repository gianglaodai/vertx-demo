package hag.prj;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.Message;

import java.util.List;

public class MainVerticle extends AbstractVerticle {
    @Override
    public void start() {
        vertx.createHttpServer().requestHandler(req->{
            vertx.eventBus().<JsonObject>rxRequest("hello",null).map(Message::body)
                    .subscribe(x->req.response().end(x.encodePrettily()),Throwable::printStackTrace);
        }).rxListen(8080).subscribe(httpServer -> System.out.println("Port 8080 ready"));
    }
}
